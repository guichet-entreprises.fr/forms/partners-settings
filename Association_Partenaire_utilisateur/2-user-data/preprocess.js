// get the user in Account
var userResponse = null;
try {
	var userResponse = nash.service.request('${account.baseUrl.read}/private/users/{mail}', $userPartner.link.userEmail) //
		.dataType('application/json') //
		.accept('json')
		.get();
} catch (e) {
	// HTTP 400 if the user does not exist
}
var user;
if (userResponse != null && userResponse.getStatus() == 200) {
	user = userResponse.asObject();
}
if (user == null || user.trackerId == null) {
	// prepare a form to create the new user
	var pwdHelp = 'Le mot de passe (au moins 8 caractères) doit respecter les 4 conditions suivantes :<ul><li>au moins un chiffre</li><li>au moins une lettre majuscule</li><li>au moins une lettre minuscule</li><li>au moins un caractère spécial !"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~</li></ul>'
	var emailValue = $userPartner.link.userEmail;
	return spec.create({ id : 'userData', groups : [
		spec.createGroup({ id : 'parameters', label : 'Paramètres', data : [
			spec.createGroup({ id : 'accountParameters', label : 'Paramètres du compte', data : [
				spec.createData({ id : 'civility',    label : 'Civilité',            type: 'ComboBox(ref:\'civilities\')', mandatory: true }),
				spec.createData({ id : 'lastName',    label : 'Nom',                 type: 'String',                       mandatory: true }),
				spec.createData({ id : 'firstName',   label : 'Prénom',              type: 'String',                       mandatory: true })
			]}),
			spec.createGroup({ id : 'connectionParameters', label : 'Paramètres de connexion', data : [
				spec.createData({ id : 'email',       label : 'Courriel',            type : 'StringReadOnly',              mandatory: true, value: emailValue }),
				spec.createData({ id : 'phone',       label : 'Numéro de téléphone', type : 'Phone',                       mandatory: true })
			]})
		]})
	]});
} else {
	// prepare a form to display the already existing user
	return spec.create({ id : 'userData', groups : [
		spec.createGroup({ id : 'parameters', label : 'Parameters', data : [
			spec.createGroup({ id : 'accountParameters', label : 'Paramètres du compte', data : [
				spec.createData({ id : 'code',        label : 'Code',                type: 'StringReadOnly',               value: user.trackerId }),
				spec.createData({ id : 'civility',    label : 'Civilité',            type: 'StringReadOnly',               value: user.civility }),
				spec.createData({ id : 'lastName',    label : 'Nom',                 type: 'StringReadOnly',               value: user.lastName }),
				spec.createData({ id : 'firstName',   label : 'Prénom',              type: 'StringReadOnly',               value: user.firstName })
			]}),
			spec.createGroup({ id : 'connectionParameters', label : 'Paramètres de connexion', data : [
				spec.createData({ id : 'email',       label : 'Courriel',            type : 'StringReadOnly',              value: user.email }),
				spec.createData({ id : 'phone',       label : 'Numéro de téléphone', type : 'StringReadOnly',              value: user.phone })
			]})
		]})
	]});
}
