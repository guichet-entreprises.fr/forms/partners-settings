var repository_id = "invitations";
var userCode;
// link the user to the partner in Directory
var role = $userPartner.link.role.id;

log.info("role is : " + role);
//Authority Code
var authorityCode = String($userPartner.link.authorityCode);
//Update Date of the invitation
var dateduJour = new Date();
var filters = [];
var updateDate = dateduJour.toISOString().slice(0,19);
log.info("updateDate is : " + updateDate);
	
log.info("authorityCode is : " + authorityCode);

log.info("Type of authorityCode is : " + typeof authorityCode);


if ($userData.parameters.accountParameters.code == null) {
	//====> Invitation managment 
	
	//Filters to find the invitation of the user 
	log.info("the email is : "+ $userData.parameters.connectionParameters.email); 
	
	filters.push("details.email:" + $userData.parameters.connectionParameters.email);
	
	// call directory with filter email to find if a user was already invited with the entred email adress
	
	var response = nash.service.request('${directory.baseUrl}/private/v1/repository/{repository}',repository_id) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000)
			   .param('filters', filters)//
	           .accept('json') //
	           .get();
	
	var foundUser = response.asObject();
	log.info("foundUser is : "+ foundUser); 
	var totalResults = !foundUser.totalResults ? null : foundUser.totalResults;
	log.info("totalResults is : "+ totalResults); 
	
	// If this email was already invited
	
	if(null != totalResults && totalResults == 1){
		log.info(" Total result is 1 which means that the user was already invited ");
		var content = !foundUser.content[0] ? null : foundUser.content[0];
		log.info("content is : " + content);
		var elementId = !content.id ? null : content.id;
		log.info("elementId is : " + elementId);
		var repositoryId = !content.repositoryId ? null : content.repositoryId;
		log.info("repositoryId is : " + repositoryId);
		var entityId = !content.entityId ? null : content.entityId;
		log.info("entityId is : " + entityId);
		var label = !content.label ? null : content.label;
		log.info("label is : " + label);
		var details = !content.details ? null : content.details;
		log.info("details is : " + details);
		//Getting the informations of the already invited user :
		
		var userId = !details.userId ? null : details.userId;
		log.info("userId is : " + userId);
		var nombreRelances = details.nombreRelances;
		log.info("nombreRelances is : " + nombreRelances);
		var civility = !details.civility ? null : details.civility;
		log.info("civility is : " + civility);
		var lastName = !details.lastName ? null : details.lastName;
		log.info("lastName is : " + lastName);
		var firstName = !details.firstName ? null : details.firstName;
		log.info("firstName is : " + firstName);
		var email = !details.email ? null : details.email;
		log.info("email is : " + email);
		var phone = !details.phone ? null : details.phone;
		log.info("phone is : " + phone);
		var authority = !details.authority ? null : details.authority;
		log.info("authority is : " + authority);
		
		log.info("nombreRelances is : " + nombreRelances);	
		
		var nombreRelancesIncremented = nombreRelances + 1;
		
		log.info("nombreRelances incremented is : " + nombreRelancesIncremented);	
		var details = {
						"userId":userId,
						"civility": civility,
						"lastName" : lastName, 
						"firstName" : firstName, 
						"email" : email, 
						"phone": phone,
						"authority":authority,
						"nombreRelances": nombreRelancesIncremented	
					  };
		
		log.info("invitation details is : " + JSON.stringify(details));	
		
		var repositoryElement = {
			"repositoryId":repositoryId,
			"entityId": entityId,
			"id":elementId,
			"label":label,
			"details":details,
			"updatedDate":updateDate
		};
		log.info("repositoryElement is : " + JSON.stringify(repositoryElement)); 
		
		log.info("Send  email");
		
		var user = {
			'civility':  civility,
			'lastName' : lastName,
			'firstName' : firstName,
			'email' :  email,
			'language' : 'fr_FR', //
			'mailType' : role == 'user' ? '0' : '1', //
			'phone': phone
		};
		
		log.info("user send to the Account Web Service is " + JSON.stringify(user));
		
		var resp = nash.service.request('${account.baseUrl.read}/private/users') //
		.dataType('application/json') //
	    .accept('json') //
		.post(JSON.stringify(user));
		
		log.info("updating the invitation ");
		
		var relanceInvitation = nash.service.request('${directory.baseUrl}/private/v1/repository/{repository}/{id}',repository_id, elementId) //
				.connectionTimeout(10000) //
				.receiveTimeout(10000) //
				.accept('application/json') //
				.put(repositoryElement);
				
	log.info("updating the invitation DONE ");
	
	user = nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}',$userPartner.link.authorityCode, userId, role) //
	.accept('text') //
	.post(null); 
	
	log.info("User added to directory ");
	
	}else{ //The user was never invited before 

	// create an user in Account
	//mailType = 0 for an simple user, 1 for a referent
	var user = {
		'civility':  $userData.parameters.accountParameters.civility.id, //
		'lastName' : $userData.parameters.accountParameters.lastName, //
		'firstName' : $userData.parameters.accountParameters.firstName, //
		'email' :  $userData.parameters.connectionParameters.email, //
		'language' : 'fr_FR', //
		'mailType' : role == 'user' ? '0' : '1', //
		'phone': $userData.parameters.connectionParameters.phone.e164
	};
	
	var resp = nash.service.request('${account.baseUrl.read}/private/users') //
		.dataType('application/json') //
	    .accept('json') //
		.post(JSON.stringify(user));
	
	logger.info("The response returned after calling account to create the user and send the email:" + resp.getStatus());
	
	// get the created user in Account
	var user = JSON.parse(resp.asString());
	log.info("User info : " + user);
	log.info("User info as string : " + JSON.stringify(user));
	// get the code of the created user
	userCode = user.trackerId;
	
		var details = {
						"userId":userCode,
						"civility":  $userData.parameters.accountParameters.civility.id, 
						"lastName" : $userData.parameters.accountParameters.lastName, 
						"firstName" : $userData.parameters.accountParameters.firstName, 
						"email" :  $userData.parameters.connectionParameters.email, 
						"phone": $userData.parameters.connectionParameters.phone.e164,
						"authority": authorityCode,
						"nombreRelances": 0		
					  };
					  
		log.info("invitation details is : " + JSON.stringify(details));	
		
		var repositoryElement = {
			"repositoryId":repository_id,
			"entityId": userCode,
			"label":"Invitation "+ userCode,
			"details":details,
			"updatedDate":updateDate
		};
		
		log.info("repositoryElement is : " + JSON.stringify(repositoryElement)); 
		
		log.info("Adding a new invitation in the repository"); 
		
			var invitation = nash.service.request('${directory.baseUrl}/private/v1/repository/{repository}',repository_id ) //
				.connectionTimeout(10000) //
				.receiveTimeout(10000) //
				.accept('application/json') //
				.post(repositoryElement);
	
	//Add the new user in directory 
		log.info("Adding the user to directory ");
		
	user = nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}',$userPartner.link.authorityCode, userCode, role) //
	.accept('text') //
	.post(null); 
	
	}	
	//<======== End of Invitation managment 
	
} else {
	// get the code of the already existing user
	userCode = $userData.parameters.accountParameters.code;
	
	user = nash.service.request('${directory.baseUrl}/v1/authority/{authorityCode}/user/{userId}/role/{role}',$userPartner.link.authorityCode, userCode, role) //
	.accept('text') //
	.post(null); 
}


		
return null;
