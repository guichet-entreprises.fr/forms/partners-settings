//Get current user identifier
var currentUserId = nash.record.description().getAuthor();

//Get authorities for the current user
var isReferent =  nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}/user/{userId}/role/{role}',
	$userPartner.link.authorityCode, currentUserId, "referent") //
					.accept('json') //
					.get() //
					.asString();

if (isReferent != "true") {
	var recordUid = nash.record.description().getRecordUid();
	log.info("Redirection to the first page for the record {}", recordUid);
	return { url: "/record/" + recordUid + "/0/page/0" };
}

return null;
