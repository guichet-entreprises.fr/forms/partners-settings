//Get current user identifier
var currentUserId = nash.record.description().getAuthor();

_log.info("currentUserId is {}", currentUserId);


 	// Get authorities for the current user
	var userAuthorities = nash.service.request('${directory.baseUrl}/v1/authority/user/{userId}', currentUserId) //
    .accept('json') //
	.get();
	
	_log.info("userAuthorities is {}", userAuthorities);

	var userAuthoritiesObject = userAuthorities.asList();
	var authorityFuncId; 
	var conftype = {
					'referential': {
									'text': []
								   }
					};	 
	_log.info("userAuthoritiesObject is {}", userAuthoritiesObject);
	_log.info("userAuthoritiesObject length is {}", userAuthoritiesObject.length);

	for (var i = 0; i < userAuthoritiesObject.length; i++) {
		authorityFuncId = userAuthoritiesObject[i].entityId;
		_log.info("authorityFuncId is {}", authorityFuncId);
		if (authorityFuncId == "GE") {
			break;
		}
		for ( var j = 0; j < userAuthoritiesObject[i].roles.length; j++) {
			_log.info("role lenght for the {} element is {}", i , userAuthoritiesObject[i].roles.length);
			if (userAuthoritiesObject[i].roles[j] === "referent") {
				conftype.referential.text.push( { 'id': authorityFuncId, 'label' : authorityFuncId} );
				_log.info("conftype is {}", conftype);
			}
		}
	} 
 	_log.info("Step after pushing data in the conftype node ");
	_log.info("conftype is {}", conftype);	
// create data.xml with information of receiver

var data = [];

var dataElement = null;
if (authorityFuncId == "GE") {
	dataElement = spec.createData({
					'id': 'authorityCode',
					'label': "Veuillez saisir l'autorité compétente pour laquelle vous désirez rajouter un utilisateur",
					'mandatory':true,
					'type': 'String'
				});
} else {
	dataElement = spec.createData({
		'id': 'authorityCode',
		'label': "Veuillez sélectionner l'autorité compétente pour laquelle vous désirez rajouter un utilisateur",
		'mandatory':true,
		'type': 'ComboBox',
		'conftype': conftype
	});
}
data.push(dataElement);

dataElement = spec.createData({
		'id': 'userEmail',
		'label': "Courriel de l'utilisateur",
		'mandatory':true,
		'type': 'Email'
	});

data.push(dataElement);

dataElement = spec.createData({
	'id': 'role',
	'label': "Rôle",
	'mandatory':true,
	'type': "ComboBox(ref:'roles')"
});

data.push(dataElement);

return spec.create({
    id : 'userPartner',
    label : "Lier un partenaire à un utilisateur",
    groups : [ spec.createGroup({
	    id : 'link',
	    help : "Il vous est demandé ici de renseigner l'autorité compétente et d'identifier l'utilisateur que vous souhaitez contacter.",
		label:"Sélection de l'autorité compétente à configurer",
	    data : data
    }) ]
});