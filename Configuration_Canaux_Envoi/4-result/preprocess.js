var authorityObject = {};
// Getting the value of the configuration in input file
var configInfos = _INPUT_.configurationChannelsPanel;
_log.info("configInfos is  {}",configInfos);
// Getting the value of the authorithy functionnal id
var authorityFuncId = $configChoicePanel.configurationChannelsChoicePanel.authority.authorityCode;
_log.info("authorityFuncId is  {}",authorityFuncId);
// Getting the value of the choice
var channelChosen = _INPUT_.configurationChannelsPanel.choiceRappel.choice;
_log.info("channelChosen is  {}",channelChosen);
//===============> get the configuration of the bloc email:
var enableEmailTransferChannel = _INPUT_.configurationChannelsPanel.email.state;
var emailAdress;
//<==============

//===============> get the configuration of the bloc backOffice:
var enableBackOfficeTransferChannel = _INPUT_.configurationChannelsPanel.backoffice.state;
//<==============

//===============> get the configuration of the bloc address:
var enableAdressTransferChannel = _INPUT_.configurationChannelsPanel.address.state;
var addressDetail={
					"recipientName": "",
					"recipientNameCompl": "",
					"addressName": "",
					"addressNameCompl": "",
					"cityName": "",
					"postalCode": ""
				};
var	recipientName = _INPUT_.configurationChannelsPanel.address.addressDetail.recipientName;
var	recipientNameCompl = _INPUT_.configurationChannelsPanel.address.addressDetail.recipientNameCompl;
var	addressName = _INPUT_.configurationChannelsPanel.address.addressDetail.addressName;
var	addressNameCompl = _INPUT_.configurationChannelsPanel.address.addressDetail.addressNameCompl;
var	cityName = _INPUT_.configurationChannelsPanel.address.addressDetail.cityName;
var	postalCode = _INPUT_.configurationChannelsPanel.address.addressDetail.postalCode;
//<==============

//===============> get the configuration of the bloc ftp:
var enableFtpTransferChannel = _INPUT_.configurationChannelsPanel.ftp.state;
var enableFtpChannel;
var ftpMode;
var pathFolder;
var type;
var comment;
var delegationAuthority;
var token;
var booleanRedirect;


//<==============
var oldTransactionChannels;
var transferChannels;
var enabledBackOfficeChannelstate = "disabled";
var enabledFtpChannelstate= "disabled";
var enabledEmailsChannelstate= "disabled";
var enabledAddressChannelstate= "disabled";
var messageDisplayed;



				
var response = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();	
_log.info("response is {}", response);
var receiverInfo = response.asObject();
_log.info("receiverInfo  is {}", receiverInfo);
var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
var contact = !receiverInfo.details ? null : receiverInfo.details;
var oldTransactionChannels = !contact.transferChannels ? null : contact.transferChannels;

if(null != oldTransactionChannels){
	//Old information from directory to be replaced if the channel is been configured because the bind in the previous step preprocess
	// Doesn't take into account the values that are not displayed in the screen when binding the data.xml
	
	var oldBackOffice = !contact.transferChannels.backoffice ? null : contact.transferChannels.backoffice;
	var oldFtp = !contact.transferChannels.ftp ? null : contact.transferChannels.ftp;
	var oldAddress = !contact.transferChannels.address ? null : contact.transferChannels.address;
	var oldEmail = !contact.transferChannels.email ? null : contact.transferChannels.email;
	  
	 _log.info("backOffice  is {}", oldBackOffice);	
	 _log.info("ftp  is {}", oldFtp);	
	 _log.info("address  is {}", oldAddress);	
	 _log.info("email  is {}", oldEmail);
	 
	emailAdress= oldEmail.emailAddress;
	
	addressDetail= oldAddress.addressDetail;
	
	ftpMode= oldFtp.ftpMode;
	pathFolder= oldFtp.pathFolder;
	comment= oldFtp.comment;
	delegationAuthority= oldFtp.delegationAuthority;
	token= oldFtp.token;

	
	_log.info("old address is  {}",addressDetail);
}				
					
authorityObject.entityId = authorityFuncId;		

	
		//{1}===> FTP Channel's configuration managment
		if(channelChosen.contains('FTP')){	
		_log.info("ENTRED THE FTP CONFIGURATION!");
			//{2} ==> Using an another authority Channel 
			_log.info("the value of choice in FTP is : {}",_INPUT_.configurationChannelsPanel.ftp.ftpChoice);
			if(Value('id').of(_INPUT_.configurationChannelsPanel.ftp.ftpChoice).eq('delegatedChannel')){
				_log.info("ENTRED THE FTP CONFIGURATION!  =====> DELEGATED CHANNEL MODE");
				enableFtpChannel = _INPUT_.configurationChannelsPanel.ftp.externalChannelChoice.state;
				_log.info("enableFtpChannel is : {}", enableFtpChannel);
				delegationAuthority = _INPUT_.configurationChannelsPanel.ftp.externalChannelChoice.authorityValue.id;
				var delegationAuthorityLabel = _INPUT_.configurationChannelsPanel.ftp.externalChannelChoice.authorityValue.label;
				if(enableFtpChannel){
					enabledFtpChannelstate = "enabled";
					messageDisplayed = "Vous avez configuré et activé le canal __FTP__, en souhaitant utiliser le canal de l'autortié : __"+delegationAuthorityLabel+"__.<br/> Vous avez la possibilité de modifier votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";			
				}else{
					messageDisplayed = "Vous avez configuré le canal __FTP__ sans l'activer, en souhaitant utiliser le canal de l'autortié : __"+delegationAuthorityLabel+"__.<br/> Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
					
					if(null != oldBackOffice){
					enabledBackOfficeChannelstate = oldBackOffice.state;
					}
					
					if(null != oldAddress){
					enabledAddressChannelstate = oldAddress.state;
					}
					
					if(null!= oldEmail){
					enabledEmailsChannelstate = oldEmail.state;
					}		
				}
				//Filling the JSON directory with the corerct values depending on the mode external channel
				ftpMode = "delegatedChannel";
				_log.info("delegationAuthority is : {}", _INPUT_.configurationChannelsPanel.ftp.externalChannelChoice.authorityValue.id);
				
				} else if (Value('id').of(_INPUT_.configurationChannelsPanel.ftp.ftpChoice).eq('push')) {
					if (Value('id').of(_INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.ftpAuthenticationChoice).eq('pushPassword')) {
											_log.info("ENTRED THE FTP CONFIGURATION!  =====> PUSHPASSWORD MODE");
											enableFtpChannel = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.state;
											_log.info("enableFtpChannel is : {}", enableFtpChannel);
	
																var eddiePassword = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.passwordEddie;
																var toUri = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.uriEddie;	
																var eddieLogin = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.loginEddie;
																var toUrl = "sftp://" + eddieLogin + '@' + toUri +"?password=${toSshPassword}&fileName={streamName}";
																comment = "FTP SERVER URL : " + toUri;
																pathFolder = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.folderPath ;
																ftpMode = "pushPassword";
																if((oldFtp == null || !oldFtp.token || oldFtp.token == "")||(oldFtp.ftpMode != "pushPassword")){
																try{
																	_log.info("Calling EDDIE WS");
																	var response = nash.service.request('${eddie.channel.create.url}/channels') //
																	.header('toSshPassword', eddiePassword) //
																	.header('toUri', toUrl) //
																	.accept('text') //
																	.post(null);
																	
																	_log.info("response status {}", response.getStatus());
																	if(response.getStatus() == 200){
																		var ftpInfo = JSON.parse(response.asString());
																		token = ftpInfo.uid ;
																		
																	}

																	_log.info("response is  {}", response.asString());
																	_log.info("ftpInfo is  {}", ftpInfo);
																	_log.info("ftpInfo uid is {}", ftpInfo.uid );
																	
																	_log.info("stepped in the try");
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error create ftp canal log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}else if (oldFtp.ftpMode == "pushPassword"){
																//If there is already a token Eddie shouuld have a functionnality to update the password that the user should have already entred in the previous step
																token = oldFtp.token;
																var eddiePassword = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.passwordEddie;
																var toUri = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.uriEddie;	
																var eddieLogin = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushPasswordChannelChoice.loginEddie;
																var toUrl = "sftp://" + eddieLogin + '@' + toUri +"?password=${toSshPassword}&fileName={streamName}";	
																var ftpCanalData = {  
																"uid": oldFtp.token,
																"toSshPassword": eddiePassword,
																"toUri" : toUrl
																};
																_log.info("ftpCanalData is  {}",ftpCanalData);
																try{
																var response = nash.service.request('${eddie.channel.create.url}/channels') //
																			   .connectionTimeout(10000) //
																			   .receiveTimeout(10000) //
																			   .dataType('application/json')//
																			   .accept('json') //
																			   .put(ftpCanalData);
																			   
																_log.info("response is  {}", response);
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error edit canal ftp log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}
	
											if(enableFtpChannel){
												enabledFtpChannelstate = "enabled";
												messageDisplayed = "Vous avez configuré et activé le canal __FTP__ sans l'activer, en souhaitant recevoir les dossier sur votre serveur FTP et en utilisant une authentification login/mot de passe.<br/> Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";			
											}else{
												messageDisplayed = "Vous avez configuré le canal __FTP__ sans l'activer, en souhaitant recevoir les dossier sur votre serveur FTP et en utilisant une authentification login/mot de passe.<br/> Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
												
												if(null != oldBackOffice){
												enabledBackOfficeChannelstate = oldBackOffice.state;
												}
												
												if(null != oldAddress){
												enabledAddressChannelstate = oldAddress.state;
												}
												
												if(null!= oldEmail){
												enabledEmailsChannelstate = oldEmail.state;
												}		
											}
					} else if (Value('id').of(_INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.ftpAuthenticationChoice).eq('pushKey')) {
											_log.info("ENTRED THE FTP CONFIGURATION!  =====> PUSHPKEY MODE");
											enableFtpChannel = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushKeyChoice.state;
											_log.info("enableFtpChannel is : {}", enableFtpChannel);

																
																var toUri = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushKeyChoice.uriEddie;	
																var eddieLogin = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushKeyChoice.loginEddie;
																pathFolder = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushKeyChoice.folderPath ;
																var toUrl = "sftp://" + eddieLogin + '@' + toUri +"?privateKeyFile=${{EddiePrivateKey}}&fileName={streamName}";
																comment = "FTP SERVER URL : " + toUri;
																ftpMode = "pushKey";
																if((oldFtp == null || !oldFtp.token || oldFtp.token == "")||(oldFtp.ftpMode != "pushKey")){
																try{
																	_log.info("Calling EDDIE WS");
																	var response = nash.service.request('${eddie.channel.create.url}/channels') //
																	.header('toUri', toUrl) //
																	.accept('text') //
																	.post(null);
																	
																	_log.info("response status {}", response.getStatus());
																	if(response.getStatus() == 200){
																		var ftpInfo = JSON.parse(response.asString());
																		token = ftpInfo.uid ;
																		_log.info("ftp info returned is {}", ftpInfo);
																	}

																	_log.info("response is  {}", response.asString());
																	_log.info("ftpInfo is  {}", ftpInfo);
																	_log.info("ftpInfo uid is {}", ftpInfo.uid );
																	
																	_log.info("stepped in the try");
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error create ftp canal log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}else if (oldFtp.ftpMode == "pushKey"){
																//If there is already a token Eddie shouuld have a functionnality to update the password that the user should have already entred in the previous step
																token = oldFtp.token;
																var toUri = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushKeyChoice.uriEddie;	
																var eddieLogin = _INPUT_.configurationChannelsPanel.ftp.pushFtpChoice.pushKeyChoice.loginEddie;
																var toUrl = "sftp://" + eddieLogin + '@' + toUri +"?privateKeyFile=${{EddiePrivateKey}}&fileName={streamName}";
																var ftpCanalData = {  
																"uid": oldFtp.token,
																"toUri" : toUrl
																};
																_log.info("ftpCanalData is  {}",ftpCanalData);
																try{
																var response = nash.service.request('${eddie.channel.create.url}/channels') //
																			   .connectionTimeout(10000) //
																			   .receiveTimeout(10000) //
																			   .dataType('application/json')//
																			   .accept('json') //
																			   .put(ftpCanalData);
																	if(response.getStatus() == 200){
																		var ftpInfo = JSON.parse(response.asString());														
																		comment = "FTP SERVER URL : " + toUri;
																		_log.info("ftp info returned is {}", ftpInfo);
																	}
																		   
																_log.info("response is  {}", response);
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error edit canal ftp log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}
											
											if(enableFtpChannel){
												enabledFtpChannelstate = "enabled";
												messageDisplayed = "Vous avez configuré et activé le canal __FTP__ , en souhaitant recevoir les dossier sur votre serveur FTP et en utilisant une authentification par clé RSA.<br/> Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";			
											}else{
												messageDisplayed = "Vous avez configuré le canal __FTP__ sans l'activer, en souhaitant recevoir les dossier sur votre serveur FTP et en utilisant une authentification par clé RSA.<br/> Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
												
												if(null != oldBackOffice){
												enabledBackOfficeChannelstate = oldBackOffice.state;
												}
												
												if(null != oldAddress){
												enabledAddressChannelstate = oldAddress.state;
												}
												
												if(null!= oldEmail){
												enabledEmailsChannelstate = oldEmail.state;
												}		
											}	
					}
				} else if (Value('id').of(_INPUT_.configurationChannelsPanel.ftp.ftpChoice).eq('pull')) {
					if (Value('id').of(_INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.ftpAuthenticationChoice).eq('pullPassword')) {
											_log.info("ENTRED THE FTP CONFIGURATION!  =====> PULLPASSWORD MODE");
											enableFtpChannel = _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullPasswordChannelChoice.state;
											_log.info("enableFtpChannel is : {}", enableFtpChannel);
																var eddiePassword = _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullPasswordChannelChoice.passwordEddie;
																ftpMode = "pullPassword";
																pathFolder = _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullPasswordChannelChoice.folderPath ;
																if((oldFtp == null || !oldFtp.token || oldFtp.token == "")||(oldFtp.ftpMode != "pullPassword")){
																try{
																	_log.info("Calling EDDIE WS");
																	var response = nash.service.request('${eddie.channel.create.url}/channels') //
																	.header('userPassword', eddiePassword) //
																	.accept('text') //
																	.post(null);
																	
																	_log.info("response status {}", response.getStatus());
																	if(response.getStatus() == 200){
																		var ftpInfo = JSON.parse(response.asString());
																		token = ftpInfo.uid ;
																		
																	}

																	_log.info("response is  {}", response.asString());
																	_log.info("ftpInfo is  {}", ftpInfo);
																	_log.info("ftpInfo uid is {}", ftpInfo.uid );
																	
																	_log.info("stepped in the try");
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error create ftp canal log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}else if (oldFtp.ftpMode == "pullPassword"){
																//If there is already a token Eddie shouuld have a functionnality to update the password that the user should have already entred in the previous step
																token = oldFtp.token;
																var eddiePassword = _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullPasswordChannelChoice.passwordEddie;
																var ftpCanalData = {  
																"uid": oldFtp.token,
																"userPassword": eddiePassword
																};
																_log.info("ftpCanalData is  {}",ftpCanalData);
																try{
																var response = nash.service.request('${eddie.channel.create.url}/channels') //
																			   .connectionTimeout(10000) //
																			   .receiveTimeout(10000) //
																			   .dataType('application/json')//
																			   .accept('json') //
																			   .put(ftpCanalData);
																			   
																_log.info("response is  {}", response);
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error edit canal ftp log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}
											
											if(enableFtpChannel){
												enabledFtpChannelstate = "enabled";
												messageDisplayed = "Vous avez configuré et activé le canal __FTP__ , en souhaitant récupérer les dossier sur le serveur FTP du service guichet entreprise et en utilisant une authentification par login/mot de passe.<br/> Le login à utiliser lors de votre connexion est : __"+token+"__. <br/>Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";			
											}else{
												messageDisplayed = "Vous avez configuré et activé le canal __FTP__, en souhaitant récupérer les dossier sur le serveur FTP du service guichet entreprise et en utilisant une authentification par login/mot de passe.<br/> Le login à utiliser lors de votre connexion est : __"+token+"__. <br/>Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
												
												if(null != oldBackOffice){
												enabledBackOfficeChannelstate = oldBackOffice.state;
												}
												
												if(null != oldAddress){
												enabledAddressChannelstate = oldAddress.state;
												}
												
												if(null!= oldEmail){
												enabledEmailsChannelstate = oldEmail.state;
												}		
											}	

											
					} else if (Value('id').of(_INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.ftpAuthenticationChoice).eq('pullKey')) {
											_log.info("ENTRED THE FTP CONFIGURATION!  =====> PULLKEY MODE");
											enableFtpChannel = _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullKeyChoice.state;
											_log.info("enableFtpChannel is : {}", enableFtpChannel);
																var eddiekey = "ssh-rsa " + _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullKeyChoice.keyEddie;
																ftpMode = "pullKey";
																pathFolder = _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullKeyChoice.folderPath ;
																if((oldFtp == null || !oldFtp.token || oldFtp.token == "") || (oldFtp.ftpMode != "pullKey")){
																try{
																	_log.info("Calling EDDIE WS");
																	var response = nash.service.request('${eddie.channel.create.url}/channels') //
																	.header('sshPublicKey', eddiekey)
																	.accept('text') //
																	.post(null);
																	
																	_log.info("response status {}", response.getStatus());
																	if(response.getStatus() == 200){
																		var ftpInfo = JSON.parse(response.asString());
																		token = ftpInfo.uid ;
																		
																	}

																	_log.info("response is  {}", response.asString());
																	_log.info("ftpInfo is  {}", ftpInfo);
																	_log.info("ftpInfo uid is {}", ftpInfo.uid );
																	
																	_log.info("stepped in the try");
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error create ftp canal log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}else if (oldFtp.ftpMode == "pullKey"){
																//If there is already a token Eddie shouuld have a functionnality to update the password that the user should have already entred in the previous step
																token = oldFtp.token;
																var eddiekey = "ssh-rsa " + _INPUT_.configurationChannelsPanel.ftp.pullFtpChoice.pullKeyChoice.keyEddie;	
																var ftpCanalData = {  
																"uid": oldFtp.token,
																"sshPublicKey": eddiekey
																};
																_log.info("ftpCanalData is  {}",ftpCanalData);
																try{
																var response = nash.service.request('${eddie.channel.create.url}/channels') //
																			   .connectionTimeout(10000) //
																			   .receiveTimeout(10000) //
																			   .dataType('application/json')//
																			   .accept('json') //
																			   .put(ftpCanalData);
																			   
																_log.info("response is  {}", response);
																}catch(e){
																	/* _log.info("stepped in the catch of eddie calls to string {}", response.toString()); */
																	_log.info("error edit canal ftp log {} ", e);
																	return spec.create({
																		id : "ManageTransferChannelsResult",
																		label : "Résultat de la gestion des canaux de transfert",
																		groups : [ spec.createGroup({
																			id : 'result',
																			label : 'Résultat de la gestion des canaux de transfert',
																			description : "Un souci est survenu lors de la configuration du canal FTP, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
																			data : []
																		}) ]
																	});	

																}
																}										
											if(enableFtpChannel){
												enabledFtpChannelstate = "enabled";
												messageDisplayed = "Vous avez configuré le canal __FTP__ sans l'activer, en souhaitant récupérer les dossier sur le serveur FTP du service guichet entreprise et en utilisant une authentification par clé RSA.<br/> Le login à utiliser lors de votre appel est : __"+token+"__. <br/>Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";			
											}else{
												messageDisplayed = "Vous avez configuré et activé le canal __FTP__, en souhaitant récupérer les dossier sur le serveur FTP du service guichet entreprise et en utilisant une authentification par clé RSA.<br/> Le login à utiliser lors de votre appel est : __"+token+"__. <br/>Vous avez la possibilité de modifier et activer votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
												
												if(null != oldBackOffice){
												enabledBackOfficeChannelstate = oldBackOffice.state;
												}
												
												if(null != oldAddress){
												enabledAddressChannelstate = oldAddress.state;
												}
												
												if(null!= oldEmail){
												enabledEmailsChannelstate = oldEmail.state;
												}		
											}
					}			
				}
		}
		//{3}===> BackOffice channel's configuration managment		
		//Check if the backOffice channel was enabled or not thus updating the state in directory	
		
		if(channelChosen.contains('backoffice')){
			_log.info("ENTRED THE BACKOFFICE CONFIGURATION!");
			if(enableBackOfficeTransferChannel){
			enabledBackOfficeChannelstate = "enabled";
			messageDisplayed = "Vous avez configuré et activé le canal __backoffice partenaires__. En cliquant sur le bouton \"Finaliser\", vos dossiers seront envoyés via ce canal. <br/><br/> Vous avez la possibilité de modifier votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";
				
			}else{
				if(null!= oldAddress){
				enabledAddressChannelstate = oldAddress.state;
				}
				
				if(null != oldFtp){
				enabledFtpChannelstate = oldFtp.state;
				}
				
				if(null!= oldEmail){
				enabledEmailsChannelstate = oldEmail.state;
				}
			}
		} 
		//{3}<==========
		
		
		//{4}===> Address channel's configuration managment		
		//Check if the channel was enabled or not thus updating the state in directory
	if(channelChosen.contains('courrier postal')){	
		_log.info("ENTRED THE ADDRESS CONFIGURATION!");

		   addressDetail={
					"recipientName": recipientName,
					"recipientNameCompl": recipientNameCompl,
					"addressName": addressName,
					"addressNameCompl": addressNameCompl,
					"cityName": cityName,
					"postalCode": postalCode
				};

				_log.info("address is  {}",addressDetail);
				
		 	if(addressNameCompl == null){
			addressNameCompl = "";
			}		
			if(recipientNameCompl == null){
			recipientNameCompl = "";
			}
			
		if(enableAdressTransferChannel){
			enabledAddressChannelstate = "enabled";
			messageDisplayed = "Vous avez configuré et activé le canal __Courrier postal__. En cliquant sur le bouton \"Finaliser\", votre courrier sera envoyé à l'adresse suivante :<br/><br/> "+recipientName+"  "+recipientNameCompl+"<br/> "+addressName+"  "+addressNameCompl+"<br/>"+postalCode+"  "+cityName+".<br/><br/> Vous avez la possibilité de modifier votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";
		}else{			
			messageDisplayed = "Vous avez configuré le canal __Courrier postal__, sans l'activer. En cliquant sur le bouton \"Finaliser\", l'adresse de réception de vos dossiers, une fois le canal activé ultérieurement, sera la suivante :<br/><br/> "+recipientName+"  "+recipientNameCompl+"<br/> "+addressName+"  "+addressNameCompl+"<br/>"+postalCode+"  "+cityName+".<br/><br/> Pour rappel, afin de recevoir vos dossiers, il est nécessaire d'activer votre canal.<br/>Vous avez la possibilité de modifier votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";
			
			if(null != oldBackOffice){
			enabledBackOfficeChannelstate = oldBackOffice.state;
			}
			
			if(null != oldFtp){
			enabledFtpChannelstate = oldFtp.state;
			}
			
			if(null!= oldEmail){
			enabledEmailsChannelstate = oldEmail.state;
			}		
			
		}
	}
	
		//{4}<==========

		//{5}===> Email channel's configuration managment		
		//Check if the email channel was enabled or not thus updating the state in directory
	if(channelChosen.contains('courrier électronique')){	
		emailAdress = _INPUT_.configurationChannelsPanel.email.emailAddress;
		_log.info("ENTRED THE EMAIL CONFIGURATION!");
		if(enableEmailTransferChannel){
			enabledEmailsChannelstate = "enabled";
			messageDisplayed = "Vous avez configuré et activé le canal __Courrier électronique__, votre courriel sera envoyé à l'adresse courriel suivante : "+emailAdress+"<br/><br/> Vous avez la possibilité de modifier votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
		}else{
			messageDisplayed = "Vous avez configuré le canal __Courrier électronique__ sans l'activer. En cliquant sur le bouton \"Finaliser\", vos dossiers, une fois le canal activé ultérieurement, seront envoyés à l'adresse courriel suivante : "+emailAdress+"<br/><br/> Vous avez la possibilité de modifier votre canal via votre espace dédié.<br/> Nous restons à votre écoute. <br/> *L'équipe du service Guichet Entreprises*.";	
		
		//Quand je configure un canal mais que je ne l'activepas je dois garder le statutactive du canal précédent
			if(null != oldBackOffice){
			enabledBackOfficeChannelstate = oldBackOffice.state;
			}
			
			if(null != oldFtp){
			enabledFtpChannelstate = oldFtp.state;
			}
			
			if(null!= oldAddress){
			enabledAddressChannelstate = oldAddress.state;
			}
		}	
	}
		//{5}<==========
var transferChannels = {
						"backoffice" :{
										"state" : enabledBackOfficeChannelstate
									},
						"email":{
								  "emailAddress" : emailAdress,
								  "state" : enabledEmailsChannelstate
							   },
						"address":{
								   "addressDetail": addressDetail,
								   "state" : enabledAddressChannelstate
								},	
						"ftp" :{
								"state": enabledFtpChannelstate,
								"ftpMode":ftpMode,
								"pathFolder":pathFolder,
								"comment":comment,
								"delegationAuthority":delegationAuthority,
								"token": token
								}	
						};
log.info("transferChannels is  {}",transferChannels);
//{6}<==========

//{7}===> Calling the WS nashto addd the "transferChannels" to the contactInfo in Directory	and thus saving the configuration of the 
// transfer channels chosen by the user.				
var contact = {}
contact["transferChannels"] = transferChannels;

authorityObject.details = contact;

_log.info("contact is  {}",contact);
_log.info("authorityObject is  {}",authorityObject);

try{
var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .dataType('application/json')//
	           .accept('json') //
	           .put(authorityObject);
}catch(e){
	_log.info("stepped in the catch of nash merge call");
					
					return spec.create({
						id : "ManageTransferChannelsResult",
						label : "Fin de la configuration",
						groups : [ spec.createGroup({
							id : 'result',
							label : 'Rappel de la configuration saisie',
							description : "Un souci est survenu lors de la sauvegarde de votre configuration, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance.",
							data : []
						}) ]
					});	

}

	_log.info("ETAPE is  {}","try result");


	return spec.create({
	id : "ManageTransferChannelsResult",
	label : "Fin de la configuration",
    groups : [ spec.createGroup({
        id : 'result',
        label : 'Rappel de la configuration saisie',
        description : messageDisplayed,
        data : []
    }) ]
});