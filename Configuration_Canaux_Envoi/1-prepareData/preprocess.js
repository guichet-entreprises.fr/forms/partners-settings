//Get current user identifier
var currentUserId = nash.record.description().getAuthor();

_log.info("currentUserId is {}", currentUserId);


 	// Get authorities for the current user
	var userAuthorities = nash.service.request('${directory.baseUrl}/v1/authority/user/{userId}', currentUserId) //
    .accept('json') //
	.get();
	
	_log.info("userAuthorities is {}", userAuthorities);

	var userAuthoritiesObject = userAuthorities.asList();
	var authorityFuncId; 
	var authorityLabel;
	var conftype = {
					'referential': {
									'text': []
								   }
					};	 
	_log.info("userAuthoritiesObject is {}", userAuthoritiesObject);
	_log.info("userAuthoritiesObject length is {}", userAuthoritiesObject.length);

	for (var i = 0; i < userAuthoritiesObject.length; i++) {
		for ( var j = 0; j < userAuthoritiesObject[i].roles.length; j++) {
			_log.info("role lenght for the {} element is {}", i , userAuthoritiesObject[i].roles.length);
			if (userAuthoritiesObject[i].roles[j] === "referent") {
				authorityFuncId = userAuthoritiesObject[i].entityId;
				
			//For each authority entityId we get the details (getting the label in this case) 	
			var directoryResponse = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
			.connectionTimeout(10000) //
		    .receiveTimeout(10000) //
			.accept('json') //
			.get();			
			
			_log.info("directoryResponse : {}", directoryResponse);			
			var receiverInfoDirectory = directoryResponse.asObject();		
			_log.info("receiverInfoDirectory is {}", receiverInfoDirectory);
			
			conftype.referential.text.push( { 'id': authorityFuncId, 'label' : receiverInfoDirectory.label} );
			_log.info("conftype is {}", conftype);
			}
		}
	} 
 	_log.info("Step after pushing data in the conftype node ");
	_log.info("conftype is {}", conftype);	
// create data.xml with information of receiver

var data = [];
var group = spec.createGroup({
		'id': "authoritiesList",
		'label': "Liste des autorités compétentes",
		'data': [
			spec.createData({
				'id': 'authority',
				'label': "Veuillez sélectionner l'autorité compétente pour laquelle vous souhaitez configurer le canal de réception des dossiers :",
				'mandatory':true,
				'type': 'ComboBox',
				'conftype': conftype
			})
		]
	});
	data.push(group);

return spec.create({
    id : 'listAuthorities',
    label : "Liste des autorités compétentes",
    groups : [ spec.createGroup({
	    id : 'authorities',
	    help : "Ci-dessous, la liste déroulante  contenant l'ensemble des autorités pour lesquelles vous êtes référent. Celle-ci vous permet ainsi de choisir l'autorité pour laquelle vous souhaitez configurer le canal de réception des dossiers.",
		label: "Sélection de l'autorité compétente à configurer",
	    data : data
    }) ]
});