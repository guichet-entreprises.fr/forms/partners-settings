_log.info("input is  {}",_INPUT_);
var authorityFuncId = _INPUT_.configurationChannelsChoicePanel.authority.authorityCode;
_log.info("authorityFuncId is  {}", authorityFuncId);
var choiceLabel = _INPUT_.configurationChannelsChoicePanel.choices.choice.label;
_log.info("choiceLabel is  {}", choiceLabel);
var contactInfo;
var authorityLabel;
var transferChannels;
var backoffice = null;
var ftp = null;
var address = null;
var email = null;

//{-}===> Functions for binding
function map(src, mapping) {
   var dst = {};
   Object.keys(mapping).forEach(function(key) {
       dst[key] = src[mapping[key]];
   });
   return dst;
}

//{-}<===

//{1}===> Get the  information of the configuration of the CMA from directories 

var authorityFuncId = Value('id').of($listAuthorities.authorities.authoritiesList.authority)._eval();
_log.info("authorityFuncId is  {}",authorityFuncId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();				   
	
_log.info("response is {}", response);
 receiverInfo = response.asObject();
_log.info("receiverInfo  is {}", receiverInfo);	

 
var contactInfo = !receiverInfo.details ? null : receiverInfo.details;
var transferChannels = {
	"backoffice": {
		"state": "disabled"
	},
	"email": {
		"emailAddress":"",
		"state": "disabled"
	},
	"address": {
		"addressDetail": {
			"recipientName": "",
			"recipientNameCompl": "",
			"addressName": "",
			"addressNameCompl": "",
			"cityName": "",
			"postalCode": ""
		},
		"state": "disabled"
	},
	"ftp": {
		"state": "disabled",
		"ftpMode":"",
		"pathFolder":"",
		"type":"",
		"comment":"",
		"delegationAuthority":"",
		"token": "",
		"booleanRedirect":""
	}
}

if(contactInfo.transferChannels == null){
	contactInfo.transferChannels = transferChannels;
}
//{1}<========== 

//(2) ===========> Get the old configuration from directory

// bloc address
 var address = contactInfo.transferChannels.address;
_log.info("address is {}", address);
//bloc backoffice 
var backoffice = contactInfo.transferChannels.backoffice;
_log.info("backoffice is {}", backoffice);
//bloc email
var email = contactInfo.transferChannels.email;
_log.info("email is {}", email);
//bloc ftp
var ftp = contactInfo.transferChannels.ftp;
_log.info("ftp is {}", ftp);
// (2) <===========
// ====> Set the choosen options for ftp channel 
var ftpChoiceBloc = "";
var ftpPushChoiceBloc = "" ;
var ftpPullChoiceBloc = "" ;
_log.info("Activated ftp mode is {}", ftp.ftpMode);
if (ftp.ftpMode == "delegatedChannel" ) {
	ftpChoiceBloc = ftp.ftpMode;
}else if (ftp.ftpMode == "pushPassword" ||  ftp.ftpMode == "pushKey" ) {
	ftpChoiceBloc = "push";
	ftpPushChoiceBloc = ftp.ftpMode;
}else if (ftp.ftpMode == "pullPassword" || ftp.ftpMode == "pullKey" ) {
	ftpChoiceBloc = "pull";
	ftpPullChoiceBloc = ftp.ftpMode;
};
_log.info("ftpChoiceBloc is {}", ftpChoiceBloc);
_log.info("ftpPushChoiceBloc is {}", ftpPushChoiceBloc);
_log.info("ftpPullChoiceBloc is {}", ftpPullChoiceBloc);
// <====
//{3}===> Change the state field so it contains YES / NO depending on the filled field ENABLED / DISABLED then binding the data.xml with the mapped values

	if(backoffice.state == "enabled"){
		backoffice.state = "yes";
	}
	
	if(ftp.state == "enabled"){
		ftp.state = "yes";
	}
	
	if(address.state == "enabled"){
		address.state = "yes";
	}
	
	if(email.state == "enabled"){
		email.state = "yes";
	}
	
	
	var data = nash.instance.load("data.xml");
	data.bind("configurationChannelsPanel",{
		choiceRappel:{
			choice:choiceLabel
		},
		backoffice:{
			state:backoffice.state
		},
		address: {
			state:address.state,
			addressDetail:{
							recipientName:address.addressDetail.recipientName , 
							recipientNameCompl:address.addressDetail.recipientNameCompl , 
							addressName:address.addressDetail.addressName,
							addressNameCompl:address.addressDetail.addressNameCompl, 
							postalCode:address.addressDetail.postalCode,
							cityName:address.addressDetail.cityName
							}	
		},
		ftp:{
			ftpChoice:{id:ftpChoiceBloc},
			externalChannelChoice:{
					state:ftp.state,
					folderPath:ftp.pathFolder,
					chosenExternalChannel:ftp.delegationAuthority 	
				},
			pushFtpChoice:{
				ftpAuthenticationChoice:{id:ftpPushChoiceBloc},
				pushPasswordChannelChoice:{
					state:ftp.state,
					folderPath:ftp.pathFolder,
					tokenFTP:ftp.token,
					coment:ftp.comment 	
				},
				pushKeyChoice:{
					state:ftp.state,
					folderPath:ftp.pathFolder,
					tokenFTP:ftp.token,
					coment:ftp.comment 	
				}
			},pullFtpChoice:{
				ftpAuthenticationChoice:{id:ftpPullChoiceBloc},
				pullPasswordChannelChoice:{
					state:ftp.state,
					folderPath:ftp.pathFolder,
					tokenFTP:ftp.token,
					coment:ftp.comment 	
				},
				pullKeyChoice:{
					state:ftp.state,
					folderPath:ftp.pathFolder,
					tokenFTP:ftp.token,
					coment:ftp.comment
				}
			}
		},
		email:{
			state:email.state,
			emailAddress:email.emailAddress
		}
	})