var choiceId="";
var choiceLabel="";
var contactInfo;
var authorityLabel;
var backOffice = null;
var ftp = null;
var address = null;
var email = null;
//Get the  information of the configuration of the CMA from directories 
var authorityFuncId = Value('id').of($listAuthorities.authorities.authoritiesList.authority)._eval();
_log.info("authorityFuncId is  {}",authorityFuncId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();				   
	
_log.info("response is {}", response);
 receiverInfo = response.asObject();
_log.info("receiverInfo  is {}", receiverInfo);	

//===> Get the authorties information
 authorityLabel = !receiverInfo.label ? null : receiverInfo.label;
_log.info("authorityLabel  is {}", authorityLabel);	
  
 contactInfo = !receiverInfo.details ? null : receiverInfo.details;
_log.info("contactInfo  is {}", contactInfo);	
 
var transferChannels = {
	"backoffice": {
		"state": "disabled"
	},
	"email": {
		"emailAddress":"",
		"state": "disabled"
	},
	"address": {
		"addressDetail": {
			"recipientName": "",
			"recipientNameCompl": "",
			"addressName": "",
			"addressNameCompl": "",
			"cityName": "",
			"postalCode": ""
		},
		"state": "disabled"
	},
	"ftp": {
		"state": "disabled",
		"ftpMode":"",
		"folderPath":"",
		"type":"",
		"comment":"",
		"overridenChannelAuthority":"",
		"token": "",
		"booleanRedirect":""
	}
}

if(contactInfo.transferChannels == null){
	contactInfo.transferChannels = transferChannels;
}
//<==========

//===> Checking the already existing configured choices	
	 
	 backOffice = !contactInfo.transferChannels.backoffice ? null : contactInfo.transferChannels.backoffice;
	 ftp = !contactInfo.transferChannels.ftp ? null : contactInfo.transferChannels.ftp;
	 address = !contactInfo.transferChannels.address ? null : contactInfo.transferChannels.address;
	 email = !contactInfo.transferChannels.email ? null : contactInfo.transferChannels.email;
	 _log.info("contactInfo  is {}", contactInfo);	
	 _log.info("backOffice  is {}", backOffice);	
	 _log.info("ftp  is {}", ftp);	
	 _log.info("address  is {}", address);	
	 _log.info("email  is {}", email);
		//Checking if the backOffice is already configured
		if(backOffice.state == "enabled"){
			 choiceId="backOffice";
			 choiceLabel="Vous souhaitez recevoir le dossier dans le backoffice partenaires";
		}
		//Checking if the FTP is already configured
		if(ftp.state == "enabled"){
			 choiceId="ftp";
			 choiceLabel="Vous souhaitez recevoir le dossier via FTP";
		}
	
		//Checking if the adress is already configured
		if(address.state == "enabled"){
			 choiceId="address";
			 choiceLabel="Vous souhaitez recevoir le dossier par courrier postal";		
		}
		//Checking if there is any email that's already configured
			if(email.state == "enabled"){
			 choiceId="email";
			 choiceLabel="Vous souhaitez recevoir le dossier par courrier électronique";			
		}

//<==========


//Load the data.xml to be binded
var data = nash.instance.load("data.xml");

data.bind("configurationChannelsChoicePanel",{
	authority:{
		authorityLabel:authorityLabel,
		authorityCode:authorityFuncId
	}
})