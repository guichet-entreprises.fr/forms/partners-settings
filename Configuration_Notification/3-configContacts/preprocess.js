_log.info("input is {}",_INPUT_);
var authorityFuncId = _INPUT_.configurationNotifications.authority.authorityCode;
_log.info("authorityFuncId is {}", authorityFuncId);
var mailinglist = _INPUT_.configurationNotifications.notifications.email;
_log.info("mailinglist is {}", mailinglist);

//{1}===> Get the  information of the configuration of the CMA from directories 
var response = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();				   

_log.info("response is {}", response);
var receiverInfo = response.asObject();
_log.info("receiverInfo  is {}", receiverInfo);	
 
var authorityLabel = !receiverInfo.label ? null : receiverInfo.label;
_log.info("authorityLabel  is {}", authorityLabel);	

receiverInfo.details.notifications = mailinglist;
//{1}<========== 

try{
	//{2}===> Persist in Directory
	var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
		.connectionTimeout(10000) //
		.receiveTimeout(10000) //
		.dataType('application/json')//
		.accept('json') //
		.put(receiverInfo);
	//{2}<==========
	
}catch(e){
	_log.error("stopped in the catch of nash merge call");
					
	return spec.create({
		id : "warning",
		label:  "Fin de la configuration",
		groups : [ spec.createGroup({
			id : 'result',
			label : 'Problème technique',
			data : [
					spec.createData({
					id: 'warn',
					label: "Erreur",
					type: 'StringReadOnly',
					mandatory: false,
					value: "Un souci est survenu lors de la sauvegarde de votre configuration, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance."
				})
			]
		}) ]
	});
}

_log.info("Merge successful");
//{3}===> Binding the data.xml with the mailing list

var data = nash.instance.load("data.xml");
data.bind("configurationNotifications",{
	authority:{
		authorityLabel:authorityLabel,
		authorityCode:authorityFuncId
	},
	notifications:{
		email:mailinglist
	}
})