var mailinglist = null;

//Get the  information of the configuration of the CMA from directories 
var authorityFuncId = Value('id').of($listAuthorities.authorities.authoritiesList.authority)._eval();
_log.info("authorityFuncId is  {}",authorityFuncId);
var response = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();				   
	
_log.info("response is {}", response);
 receiverInfo = response.asObject();
_log.info("receiverInfo  is {}", receiverInfo);	

//===> Get the authorties information
 authorityLabel = !receiverInfo.label ? null : receiverInfo.label;
_log.info("authorityLabel  is {}", authorityLabel);	
  
 contactInfo = !receiverInfo.details ? null : receiverInfo.details;
_log.info("contactInfo  is {}", contactInfo);	
 
//<==========

//===> Checking the already existing configured choices	
	 
mailinglist = !contactInfo.notifications ? [] : contactInfo.notifications;
 
_log.info("contactInfo  is {}", contactInfo);	
_log.info("mailinglist  is {}", mailinglist);

//<==========

//Load the data.xml to be binded
var data = nash.instance.load("data.xml");

data.bind("configurationNotifications",{
	authority:{
		authorityLabel:authorityLabel,
		authorityCode:authorityFuncId
	},
	notifications:{
		email:mailinglist
	}
})