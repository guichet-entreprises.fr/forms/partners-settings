var gr_authorityInformation = _INPUT_.updateAuthority.authorityInformation;
_log.info("gr_authorityInformation is  {}",gr_authorityInformation);
var gr_ProfilInformation = _INPUT_.updateAuthority.profilInformation;
_log.info("ProfilInformation is  {}",gr_ProfilInformation);


var newDataProfil = {
					"tel": gr_ProfilInformation.telValue ? gr_ProfilInformation.telValue : "" ,
				    "fax": gr_ProfilInformation.faxValue ? gr_ProfilInformation.faxValue : "" ,
				    "email": gr_ProfilInformation.emailValue ? gr_ProfilInformation.emailValue : "" ,
				    "url": gr_ProfilInformation.urlValue ? gr_ProfilInformation.urlValue : "" ,
				    "observation": gr_ProfilInformation.observationValue,

						"address": {
						    "recipientName": gr_ProfilInformation.addressInformations.recipientNameValue ? gr_ProfilInformation.addressInformations.recipientNameValue : "",
						    "addressNameCompl": gr_ProfilInformation.addressInformations.addressNameComplValue ? gr_ProfilInformation.addressInformations.addressNameComplValue : "",
						    "compl": gr_ProfilInformation.addressInformations.complValue ? gr_ProfilInformation.addressInformations.complValue : "",
						    "special": gr_ProfilInformation.addressInformations.specialValue ? gr_ProfilInformation.addressInformations.specialValue : "",
						    "postalCode": gr_ProfilInformation.addressInformations.postalCodeValue ? gr_ProfilInformation.addressInformations.postalCodeValue : "",
						    "cedex": gr_ProfilInformation.addressInformations.cedexValue ? gr_ProfilInformation.addressInformations.cedexValue : "",
						    "cityNumber": gr_ProfilInformation.addressInformations.cityNumberValue.id ? gr_ProfilInformation.addressInformations.cityNumberValue.id : ""
						            }
  
						};
log.info("newDataProfil is  {}",newDataProfil);

// Calling the WS nash to update the details information in Directory.
var profil = {}
profil["profile"] = newDataProfil;
var authorityObject = {};
authorityObject.entityId = gr_authorityInformation.entityIdValue;
authorityObject.details = profil;

_log.info("profil is  {}",profil);
_log.info("authorityObject is  {}",authorityObject);

try{
var response = nash.service.request('${directory.baseUrl}/v1/authority/merge') //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .dataType('application/json')//
	           .accept('json') //
	           .put(authorityObject);

//Load the data.xml to be binded
var data = nash.instance.load("data.xml");

// Convert Int to string
if (gr_ProfilInformation.addressInformations.postalCodeValue){
	var postalCodeValue = gr_ProfilInformation.addressInformations.postalCodeValue.toString();
}
if (gr_ProfilInformation.addressInformations.cedexValue){
	var cedexValue= gr_ProfilInformation.addressInformations.cedexValue.toString();
}
if (gr_ProfilInformation.addressInformations.cityNumberValue){
	var cityNumberValue = gr_ProfilInformation.addressInformations.cityNumberValue.label;
}


data.bind("updateAuthority",{

	authorityInformation:{
		entityIdValue:gr_authorityInformation.entityIdValue,
		labelValue:gr_authorityInformation.labelValue,
	},
		
	profilInformation:{
			telValue:gr_ProfilInformation.telValue ? gr_ProfilInformation.telValue : "",
			faxValue:gr_ProfilInformation.faxValue ? gr_ProfilInformation.faxValue : "",
			emailValue:gr_ProfilInformation.emailValue ? gr_ProfilInformation.emailValue.toString() : "",
			urlValue:gr_ProfilInformation.urlValue ? gr_ProfilInformation.urlValue.toString() : "",
			observationValue:gr_ProfilInformation.observationValue ? gr_ProfilInformation.observationValue : "",

			addressInformations:{
				recipientNameValue:gr_ProfilInformation.addressInformations.recipientNameValue ? gr_ProfilInformation.addressInformations.recipientNameValue : "",
				addressNameComplValue:gr_ProfilInformation.addressInformations.addressNameComplValue ? gr_ProfilInformation.addressInformations.addressNameComplValue : "",
				complValue:gr_ProfilInformation.addressInformations.complValue ? gr_ProfilInformation.addressInformations.complValue : "",
				specialValue:gr_ProfilInformation.addressInformations.specialValue ? gr_ProfilInformation.addressInformations.specialValue : "",
				postalCodeValue:gr_ProfilInformation.addressInformations.postalCodeValue ? gr_ProfilInformation.addressInformations.postalCodeValue.toString() : "",
				cedexValue:gr_ProfilInformation.addressInformations.cedexValue ? gr_ProfilInformation.addressInformations.cedexValue.toString() : "",
				cityNumberValue: cityNumberValue ? cityNumberValue : ""

			}
	}

	

});


}catch(e){
	_log.error("stepped in the catch of nash merge call");
					
					return spec.create({
						id : "warning",
						label:  "Fin de la configuration",
						groups : [ spec.createGroup({
							id : 'result',
							label : 'Problème technique',
							data : [
									spec.createData({
									id: 'warn',
									label: "Erreur",
									type: 'StringReadOnly',
									mandatory: false,
									value: "Un souci est survenu lors de la sauvegarde de votre configuration, veuillez contacter le support \"support.guichet-entreprises@helpline.fr\" pour obtenir une assistance."
								})

							]
						}) ]
					});	

}