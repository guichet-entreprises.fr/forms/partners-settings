//Get current user identifier
var currentUserId = nash.record.description().getAuthor();

_log.info("currentUserId is {}", currentUserId);


 	// Get authorities for the current user
	var userAuthorities = nash.service.request('${directory.baseUrl}/v1/authority/user/{userId}', currentUserId) //
    .accept('json') //
	.get();
	
	_log.info("userAuthorities is {}", userAuthorities.asList());

	var userAuthoritiesObject = userAuthorities.asList();
	var authorityFuncId; 
	var conftype = {
					'referential': {
									'text': []
								   }
					};	 
	_log.info("userAuthoritiesObject is {}", userAuthoritiesObject);
	_log.info("userAuthoritiesObject length is {}", userAuthoritiesObject.length);

	for (var i = 0; i < userAuthoritiesObject.length; i++) {
		authorityFuncId = userAuthoritiesObject[i].entityId;
		_log.info("authorityFuncId is {}", authorityFuncId);
		if (authorityFuncId == "GE") {
			break;
		}
		for ( var j = 0; j < userAuthoritiesObject[i].roles.length; j++) {
			_log.info("role lenght for the {} element is {}", i , userAuthoritiesObject[i].roles.length);
			if (userAuthoritiesObject[i].roles[j] === "referent") {
				


//For each authority entityId we get the details (getting the label in this case) 	
			var directoryResponse = nash.service.request('${directory.baseUrl}/v1/authority/{entityId}', authorityFuncId) //
			.connectionTimeout(10000) //
		    .receiveTimeout(10000) //
			.accept('json') //
			.get();			
			
			_log.info("directoryResponse : {}", directoryResponse);			
			var receiverInfoDirectory = directoryResponse.asObject();		
			_log.info("receiverInfoDirectory is {}", receiverInfoDirectory);
			
			conftype.referential.text.push( { 'id': authorityFuncId, 'label' : receiverInfoDirectory.label} );
			_log.info("conftype is {}", conftype);

			}
		}
	} 
 	_log.info("Step after pushing data in the conftype node ");
	_log.info("conftype is {}", conftype);	
// create data.xml with information of receiver

var data = [];

var dataElement = null;
if (authorityFuncId == "GE") {
	dataElement = spec.createData({
					'id': 'authorityCode',
					'label': "Veuillez saisir l'autorité à modifier",
					'mandatory':true,
					'type': 'String'
				});
} else {
	dataElement = spec.createData({
		'id': 'authorityCode',
		'label': "Veuillez sélectionner l'autorité à modifier",
		'mandatory':true,
		'type': 'ComboBox',
		'conftype': conftype
	});
}
data.push(dataElement);


return spec.create({
    id : 'userPartner',
    label : "Modification de l'autorité sélectionnée",
    groups : [ spec.createGroup({
	    id : 'link',
	    help : "Il vous est demandé ici de sélectionner l'autorité dont vous voulez modifier les informations.",
		label:"Sélection de l'autorité",
	    data : data
    }) ]
});