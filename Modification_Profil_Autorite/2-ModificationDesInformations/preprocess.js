var form = _INPUT_;
_log.info("form is  {}",_INPUT_);

if(form.link.authorityCode.id){}

var entityIdValue =! form.link.authorityCode.id ? String(form.link.authorityCode) : String(form.link.authorityCode.id);

//var entityIdValue = String(form.link.authorityCode.id);
_log.info("idAuthority is  {}",entityIdValue);



// call directory with funcId to find all information of Authorithy
var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', entityIdValue) //
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .accept('json') //
	           .get();

//result			     
var receiverInfo = response.asObject();

var labelValue = receiverInfo.label;
_log.info("labelAuthority is  {}",labelValue);

var details = receiverInfo.details;
log.info("details is  {}",details);
var profileInfo = details.profile;
log.info("profile is  {}",profileInfo);
var addressInfo = profileInfo.address;
_log.info("address is  {}",addressInfo);

//var déclaration
var telValue ;
var faxValue ;
var emailValue ;
var urlValue;
var observationValue ;
var recipientNameValue ;
var addressNameComplValue ;
var complValue;
var specialValue ;
var postalCodeValue ;
var cedexValue ;
var cityNumberValue ;
	
//prepare all information of receiver to create data.xml
if (profileInfo){
	
	telValue =! profileInfo.tel ? null : profileInfo.tel;
	faxValue =! profileInfo.fax ? null : profileInfo.fax;
	emailValue =! profileInfo.email ? null : profileInfo.email;
	urlValue =! profileInfo.url ? null : profileInfo.url;
	observationValue =! profileInfo.observation ? null : profileInfo.observation;
	if (addressInfo){
		recipientNameValue =! addressInfo.recipientName ? null : addressInfo.recipientName;
		addressNameComplValue =! addressInfo.addressNameCompl ? null : addressInfo.addressNameCompl;
		complValue =! addressInfo.compl ? null : addressInfo.compl;
		specialValue =! addressInfo.special ? null : addressInfo.special;
		postalCodeValue =! addressInfo.postalCode ? null : addressInfo.postalCode.toString();
		cedexValue =! addressInfo.cedex ? null : addressInfo.cedex;
		
		
		if(addressInfo.cityNumber == 'null'){
			cityNumberValue = null;
		}else{
			
			var filters = [];
			filters.push("details.codepostal:" + addressInfo.postalCode);
			/*
			var orders = [];
			orders.push("details.codepostal:label:asc");
			*/
			
			var responseCity = nash.service.request('${directory.baseUrl}/v2/ref/list/zipcode')
	           .connectionTimeout(10000) //
	           .receiveTimeout(10000) //
	           .param('filters', filters)//
	            //.param('orders', orders)//
	           .param('maxResults',999)//
	           .accept('json') //
	           .get();
			var receiverCityObject = responseCity.asObject();
			_log.info("receiverCityObject is {}", receiverCityObject);
			var totalResultCity= receiverCityObject.totalResults;
			
			_log.info("totalResultCity is {}", totalResultCity);
			var cityNumberValueId;
			var cityNumberValueLabel;
			for(var k = 0; k < totalResultCity; k++){
				if(receiverCityObject.content[k].entityId == addressInfo.cityNumber){
					cityNumberValueId =receiverCityObject.content[k].entityId;
					cityNumberValueLabel = receiverCityObject.content[k].label ;
					break;
				}
				
			}
			
		}	
	}

}

log.info("cityNumberValueId : " + cityNumberValueId);
log.info("cityNumberValueLabel : " + cityNumberValueLabel);

//Load the data.xml to be binded
var data = nash.instance.load("data.xml");
if (undefined !== cityNumberValueId && undefined !== cityNumberValueLabel) {
	data.bind("updateAuthority",{
		profilInformation:{
			addressInformations:{
				cityNumberValue:{
					id: cityNumberValueId,
					label: cityNumberValueLabel
				}
			}
		}
	});
}
data.bind("updateAuthority",{
	authorityInformation:{
		entityIdValue:entityIdValue,
		labelValue:labelValue,
	},	
	profilInformation:{
			telValue:telValue,
			faxValue:faxValue,
			emailValue:emailValue,
			urlValue:urlValue,
			observationValue:observationValue,

			addressInformations:{
				recipientNameValue:recipientNameValue,
				addressNameComplValue:addressNameComplValue,
				complValue:complValue,
				specialValue:specialValue,
				postalCodeValue:postalCodeValue,
				cedexValue:cedexValue
			}
	}
});